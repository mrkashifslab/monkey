const nconf = require('nconf')
const axios = require('axios')
const crypto = require('crypto')

const baseUrl = nconf.get('elemental:url')
// const api_key = nconf.get('elemental:api_key')
// const secret_key = nconf.get('elemental:secret_key')

// const baseUrl = 'https://tapp.elemental.financial/'
// const api_key = 'fc33d26e-00e3-4c35-82df-5e786efd4491'
// const secret_key = 'ff5e9fc6-1094-400a-98cf-a3fb4ec8db12'

function encodeStr(queryObj, nesting = '') {
  const pairs = Object.entries(queryObj).map(([key, value]) => {
    if (typeof value === 'object') return `${key}=${JSON.stringify(value)}`
    else return `${key}=${value}`
  })

  return pairs.join('&')
}

const preVerify = async (docketId, elementalClient) => {
  let api_key = elementalClient.api_key,
    secret_key = elementalClient.secret_key

  const method = 'PUT',
    path = `external_clients/docket/${docketId}/pre_verify`,
    url = `${baseUrl + path}`,
    tonce = new Date().getTime() + 1000,
    params = {
      api_token: api_key,
      tonce,
      id: docketId
    }

  const signature = crypto
    .createHmac('sha256', secret_key)
    .update(`${method}|${path}|api_token=${params.api_token}&id=${params.id}&tonce=${tonce}`)
    .digest('hex')

  params.signature = signature

  try {
    const urlWithParams = `${url}?api_token=${params.api_token}&tonce=${tonce}&signature=${signature}`

    console.log('url', urlWithParams);

    const res = await axios[method.toLowerCase()](`${urlWithParams}`, params)

    return res.data
  } catch (err) {
    throw err.response.data
  }
}

module.exports = {
  preVerify
}

// preVerify(800).then(console.log).catch(console.log)
