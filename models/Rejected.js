const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const RejectedQueueSchema = new mongoose.Schema({}, {
  collection: 'rejected-queue',
  strict: false,
  timestamps: true
});

module.exports = mongoose.model('RejectedQueue', RejectedQueueSchema);
