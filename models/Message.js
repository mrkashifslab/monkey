const mongoose = require('mongoose')
mongoose.Promise = global.Promise

const MessageSchema = new mongoose.Schema(
  {},
  {
    strict: false,
    timestamps: true
  }
)

module.exports = mongoose.model('Message', MessageSchema)
