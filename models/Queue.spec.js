const mongoose = require("mongoose");
const { MongoMemoryServer } = require("mongodb-memory-server");
const connectDatabase = require("../config/initializers/database");

const Queue = require("../models/Queue");

let MainMongoServer;

describe("Queue model init tests", () => {
  beforeAll(async () => {
    require("../config/initializers/environment")();
    MainMongoServer = new MongoMemoryServer();
    const mongoUri = await MainMongoServer.getConnectionString();
    await connectDatabase(mongoUri, {
      useNewUrlParser: true
    });
  });
  //   afterEach(async () => {
  //     mongoose.disconnect();
  //   });

  afterAll(async () => {
    mongoose.disconnect();
    await MainMongoServer.stop();
  });

  test("Should be valid", () => {
    const QueueModel = new Queue({
      message: "hello",
      consumed: true,
      processed: true,
      payload: {
        sent: "Aashish",
        received: 1
      },
      consumer: {
        name: "harish",
        task: "random"
      }
    });
    QueueModel.validate(function(err) {
      expect(err).toBe(null);
    });
  });
  test("Should have validation error if some required fields is missing", () => {
    const QueueModel = new Queue({
      message: "hello",
      processed: true,
      payload: {
        sent: "Aashish",
        received: 1
      },
      consumer: {
        name: "harish",
        task: "random"
      }
    });
    QueueModel.validate(function(err) {
      expect(err).toBeInstanceOf(Error);
      expect(err.message).toEqual(
        "Queue validation failed: consumed: Path `consumed` is required."
      );
    });
  });

  it("should insert a doc into collection", async () => {
    const mock = {
      payload: { sent: "Aashish", received: 1 },
      consumer: { name: "harish", task: "random" },
      parent: null,
      _id: "5cf0b71fee2af7ffb96aaadb",
      message: "hello",
      processed: true,
      consumed: true,
      __v: 0
    };
    const QueueModel = new Queue(mock);
    await QueueModel.save();
    let inserted = await Queue.findOne({ message: "hello" });
    expect(inserted.message).toEqual("hello");
  });
});
