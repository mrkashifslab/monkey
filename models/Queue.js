const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const QueueSchema = new mongoose.Schema(
  {
    message: {
      type: String,
      required: true
    },
    consumed: {
      type: Boolean,
      required: true
    },
    processed: {
      type: Boolean,
      required: true
    },
    parent: {
      type: String,
      // required: true,
      default: null
    },
    payload: {
      sent: {
        type: mongoose.Schema.Types.Mixed,
        default: null
        //   required: true
      },
      received: {
        type: mongoose.Schema.Types.Mixed,
        default: null
        // required: true
      }
    },
    consumer: {
      name: {
        type: String
      },
      task: {
        type: String
      }
    }
  },
  {
    collection: "queue",
    timestamps: true
  }
);

module.exports = mongoose.model("Queue", QueueSchema);
