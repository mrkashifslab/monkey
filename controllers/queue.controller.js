const mongoose = require('mongoose')
const MessageModel = mongoose.model('Message')
const QueueModel = mongoose.model('Queue')
const elementalOps = require('../shared/elemental')

const unprocessedTask = async (req, res) => {
  try {
    const tasks = await QueueModel.find({
      processed: false
    })
    res.status(200).json({
      status: true,
      data: tasks
    })
  } catch (err) {
    res.status(400).json({
      status: false,
      message: err.message
    })
  }
}

const completeTask = async (req, res) => {
  try {
    const task = await QueueModel.findOneAndUpdate(
      {
        _id: req.params.taskId,
        processed: false
      },
      {
        processed: true
      },
      {
        new: true
      }
    )

    res.json({
      status: true,
      message: 'task processing completed'
    })

    if (task) {
      const taskData = task._doc

      Promise.all([
        MessageModel.findById(taskData.message),
        QueueModel.find({
          message: taskData.message
        })
      ])
        .then(([message, tasks]) => {
          message = message._doc
          tasks = tasks.map(task => task._doc)
          const docketId = message.docket.id
          const checkAllTasksProcessed = tasks.map(task => task.processed).every(task => task)
          const elementalClient = message.profile[0]

          if (checkAllTasksProcessed) {
            elementalOps
              .preVerify(docketId, elementalClient)
              .then(res => {
                console.log('elemental preverified docket id -', docketId)
              })
              .catch(err => console.error('error in elemental pre verify -', docketId, '- err -', err))
          }
        })
        .catch(err => console.error(err))
    }
  } catch (err) {
    res.status(400).json({
      status: false,
      message: 'task not found in queue'
    })
  }
}

module.exports = {
  completeTask,
  unprocessedTask
}
