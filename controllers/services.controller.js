const schema = require('../config/services/schema.json')

const allServices = (req, res) => {
  if (schema) res.status(200).json({ status: true, data: schema })
  else res.status(404).json({ status: false, message: 'No services found' })
}

const singleService = (req, res) => {
  if (schema) {
    const service = schema.find(service => {
      return service.serviceName == req.params.service
    })
    if (service) return res.status(200).json({ status: true, data: service })
  }
  return res.status(404).json({ status: false, message: 'Service not found' })
}

module.exports = {
  allServices,
  singleService
}
