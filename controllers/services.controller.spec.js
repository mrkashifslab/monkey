const mockRequest = params => ({
  params
})

const mockResponse = () => {
  const res = {}
  res.status = jest.fn().mockReturnValue(res)
  res.json = jest.fn().mockReturnValue(res)
  return res
}

describe('Dashboard controller test', () => {
  let services

  beforeAll(async () => {
    require('../config/initializers/environment')()

    services = require('./services.controller')
  })

  test('Should return array of all services and status 200', async () => {
    const res = mockResponse()

    try {
      await services.allServices('dad', res)

      expect(res.json).toHaveBeenCalledWith(expect.objectContaining({ status: true }))
      expect(res.status).toHaveBeenCalledWith(200)
    } catch (err) {
      console.log(err)
    }
  })
  test('Should not return a single serivice and should 404 status', async () => {
    const res = mockResponse()
    const req = mockRequest({ service: 'au1adad0tix' })

    try {
      await services.singleService(req, res)

      expect(res.json).toHaveBeenCalledWith(expect.objectContaining({ status: false }))
      expect(res.status).toHaveBeenCalledWith(404)
    } catch (err) {
      console.log(err)
    }
  })

  test('Should return a single serivice and 200 status', async () => {
    const res = mockResponse()
    const req = mockRequest({ service: 'au10tix' })

    try {
      await services.singleService(req, res)

      expect(res.json).toHaveBeenCalledWith(expect.objectContaining({ status: true }))
      expect(res.status).toHaveBeenCalledWith(200)
    } catch (err) {
      console.log(err)
    }
  })
})
