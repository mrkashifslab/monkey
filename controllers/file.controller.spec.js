const { downloadFile, deleteFile } = require("./file.controller");
const fs = require("fs");
const nconf = require("nconf");
const request = require("supertest");
const express = require("express");
const mockServer = require("../test/helpers/mockServer.helper");

const instance = new mockServer(5555);
const mockRequest = params => ({
  params
});

const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};

describe("File Handler - Helps in upload and download file", () => {
  beforeAll(async () => {
    require("../config/initializers/environment")();
    await instance.start();
  });

  afterAll(async () => {
    await instance.stop();
  });

  test("It should get 404 file not found", () => {
    const req = mockRequest({ fileName: "passportNew" });
    const res = mockResponse();
    downloadFile(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
  });

  test("It Upload the file and return name of it", () => {
    const filePath = nconf.get("dirs:receive") + "passport1.pdf";

    if (fs.existsSync(filePath)) {
      console.log("herrere");

      request(instance.app)
        .post("/api/file")
        .attach("file", filePath)
        .expect(200)
        .then(res => {
          expect(res.body.status).toEqual(true);
          expect(
            fs.existsSync(nconf.get("dirs:uploads") + res.body.name)
          ).toEqual(true);
        })
        .catch(err => {
          console.log(err);
        });
    }
  });

  test("Test should return status false multiple file cant be upload at once", () => {
    const filePath = nconf.get("dirs:receive") + "passport1.pdf";

    if (fs.existsSync(filePath)) {
      request(instance.app)
        .post("/api/file")
        .attach("file", filePath)
        .attach("file", filePath)
        .expect(400)
        .then(res => {
          expect(res.body.status).toEqual(false);
        })
        .catch(err => {
          console.log("multiple files upload error ", err);
        });
    }
  });

  test("Test should   return  status false wrong field name is send files are sent", () => {
    const filePath = nconf.get("dirs:receive") + "passport1.pdf";

    if (fs.existsSync(filePath)) {
      request(instance.app)
        .post("/api/file")
        .attach("ll", filePath)
        .expect(400)
        .then(res => {
          expect(res.body.status).toEqual(false);
        })
        .catch(err => {
          console.log("Wrong field name error", err);
        });
    }
  });

  test("It Should delete the file", () => {
    const req = mockRequest({ fileName: "passport" });
    const res = mockResponse();
    deleteFile(req, res);
    const filePath = nconf.get("dirs:uploads") + "passport";
    expect(fs.existsSync(filePath)).toEqual(false);
  });

  test("It Should return with 404 if file does not exist", () => {
    const req = mockRequest({ fileName: "passportw" });
    const res = mockResponse();
    deleteFile(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
  });

  test("It Should return with 400 if there are no query parameters", () => {
    const req = mockRequest({});
    const res = mockResponse();
    deleteFile(req, res);
    expect(res.status).toHaveBeenCalledWith(400);
  });

  // test("It should download the file", () => {
  //   const req = mockRequest({ fileName: "passport" });
  //   const res = mockResponse();

  //   downloadFile(req, res);
  //   console.log(res);
  // });
});
