const Joi = require("joi");
const mongoose = require("mongoose");
const provideClientReportCron = require("../config/crons/provideClientReport");
const consumers = require("../consumers");
const availableConsumers = Object.keys(consumers).map(consumer =>
  consumer.toLowerCase()
);
const { isString } = require("../util/fn.util");
const MessageModel = mongoose.model("Message");

const incoming = async (req, res) => {
  const type = Object.keys(req.body)[0];

  let body;
  if (isString(req.body[type])) body = JSON.parse(req.body[type]);
  else body = req.body[type];

  if (type === "client") body.clientId = body.id;
  else if (type === "company") body.companyId = body.id;
  else if (type === "business_transaction")
    body.businessTransactionId = body.id;

  let elementalClient;

  if (type === "client" || type === "company")
    elementalClient = require("../config/elemental/client_profiles.json")[
      body.profiles[0].id
    ];

  if (type === "business_transaction")
    elementalClient = require("../config/elemental/client_profiles.json")[
      body.profile.id
    ];

  if (elementalClient) {
    delete elementalClient.name;

    body.profiles[0] = {
      ...body.profiles[0],
      ...elementalClient
    };

    try {
      const message = await MessageModel(body).save();

      res.json({
        status: true,
        id: message._doc._id
      });

      if (type !== "business_transaction")
        availableConsumers.forEach(consumer => {
          consumers[consumer].messageParser(message._doc);
        });

      provideClientReportCron.start();

      return;
    } catch (err) {
      if (!res.headersSent)
        res.status(500).json({
          status: false,
          message: "something went wrong, try again."
        });

      return;
    }
  }

  console.log(
    "elemental client -",
    body.profiles[0].id,
    "-",
    body.profiles[0].name,
    "- unavailable"
  );
  res.status(404).json({
    status: false,
    message: "client profile not found"
  });
};

module.exports = {
  incoming
};
