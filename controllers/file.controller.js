const multer = require("multer");
const nconf = require("nconf");
const fs = require("fs");
const uuidv4 = require("uuid/v4");

const deleteFile = async (req, res) => {
  if (req.params.fileName == undefined) {
    return res.status(400).json({
      status: false,
      message: "filename required"
    });
  }

  const path = nconf.get("dirs:uploads") + req.params.fileName;
  try {
    await fs.unlinkSync(path);
    res.status(200).json({
      status: true,
      message: "File deleted Succesfully"
    });
  } catch (err) {
    return res.status(404).json({
      status: false,
      message: err.message
    });
  }
};

const uploadFile = function(req, res) {
  const maxFilesAllowed = 1;

  // define multer options for form upload
  const multerOptions = {
    preservePath: true,
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, nconf.get("dirs:uploads"));
      },
      filename: (req, file, cb) => {
        cb(
          null,
          uuidv4() + "-" + Date.now() + "." + file.mimetype.split("/")[1]
        );
      }
    }),
    limits: {
      files: maxFilesAllowed
    }
  };

  const upload = multer(multerOptions).fields([
    {
      name: "file",
      maxCount: 1
    }
  ]);

  upload(req, res, err => {
    if (err instanceof multer.MulterError) {
      return res.status(400).json({
        status: false,
        message: err.message + "  +  Somethign went wrong pre uploading "
      });
    } else if (err) {
      // An unknown error occurred when uploading.

      res.status(400).json({
        status: false,
        message: err.message + " + Something went wrong during uploading"
      });
    }

    if (Object.keys(req.files).length === 0) {
      return res.status(400).json({
        status: false,
        message: "Some fields are missing!"
      });
    } else {
      res.status(200).json({
        status: true,
        name: req.files.file[0].filename
      });
      // Populate body with file location
    }
  });
};

const downloadFile = (req, res) => {
  const filePath = `${nconf.get("dirs:uploads")}/${req.params.fileName}`;

  fs.existsSync(filePath)
    ? res.download(filePath)
    : res.status(404).json({ message: "File not exists", status: false });
};

module.exports = {
  uploadFile,
  downloadFile,
  deleteFile
};
