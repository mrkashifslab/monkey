const mongoose = require('mongoose')
const { MongoMemoryServer } = require('mongodb-memory-server')
const connectDatabase = require('../config/initializers/database')
const Queue = require('../models/Queue')

const mockRequest = params => ({
  params
})

const mockResponse = () => {
  const res = {}
  res.status = jest.fn().mockReturnValue(res)
  res.json = jest.fn().mockReturnValue(res)
  return res
}

describe('Dashboard controller test', () => {
  let queueController
  let MainMongoServer

  beforeAll(async () => {
    require('../config/initializers/environment')()
    MainMongoServer = new MongoMemoryServer()
    const mongoUri = await MainMongoServer.getConnectionString()
    await connectDatabase(mongoUri, {
      useNewUrlParser: true
    })
    dashboard = require('./queue.controller')
  })

  afterAll(async () => {
    mongoose.disconnect()
    await MainMongoServer.stop()
  })

  test('Should return array of unprocessed task and status 200', async () => {
    const mock = {
      payload: { sent: 'test', received: 1 },
      consumer: { name: 'test', task: 'random' },
      parent: null,
      _id: '5cf0b71fee2af7ffb96aaadb',
      message: 'hello',
      processed: false,
      consumed: true,
      __v: 0
    }
    const QueueModel = new Queue(mock)
    await QueueModel.save()
    const res = mockResponse()

    try {
      await queueController.unprocessedTask('dad', res)

      expect(res.json).toHaveBeenCalledWith(expect.objectContaining({ status: true }))
      expect(res.status).toHaveBeenCalledWith(200)
    } catch (err) {
      console.log(err)
    }
  })
})
