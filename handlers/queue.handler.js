const nconf = require('nconf')
const mongoose = require('mongoose')
const QueueModel = mongoose.model('Queue')
const RejectedQueueModel = mongoose.model('RejectedQueue')
const socket = require('../config/initializers/sockets')

const toQueue = async (taskData, beforeSave) => {
  try {
    const task = await QueueModel(taskData)
    if (beforeSave) beforeSave(task)
    task.save()

    // Sending message to socket client
    await socket.sendMessage(nconf.get('socket:queueEvent'), task._doc)
    return task._doc
  } catch (err) {
    console.error('Error queuing parsed message - ', err)
  }
}

const rejectQueue = data => {}

module.exports = {
  toQueue
}
