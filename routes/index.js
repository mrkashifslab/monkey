const router = require('express').Router()
const { catchErrors } = require('../helpers/error')
const messageController = require('../controllers/message.controller')
const queueController = require('../controllers/queue.controller')
const fileController = require('../controllers/file.controller')
const servicesController = require('../controllers/services.controller')

router.get('/ping', (req, res) => {
  res.status(200).json({
    message: 'pong'
  })
})

router.post('/api/message', catchErrors(messageController.incoming))

router.post('/api/file', fileController.uploadFile)

router.delete('/api/file/:fileName', fileController.deleteFile)

router.get('/api/file/:fileName', fileController.downloadFile)

router.get('/api/queue/task', queueController.unprocessedTask)

router.put('/api/queue/:taskId', queueController.completeTask)

router.get('/api/services', servicesController.allServices)

router.get('/api/services/:service', servicesController.singleService)

module.exports = router
