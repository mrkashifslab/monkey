const nconf = require('nconf')
const axios = require('axios')

const au10tixInstance = axios.create({
  baseURL: nconf.get('microservices:au10tix')
})

const complyAdvantageInstance = axios.create({
  baseURL: nconf.get('microservices:comply_advantage')
})

const emailageInstance = axios.create({
  baseURL: nconf.get('microservices:emailage')
})

const instances = {
  au10tixInstance,
  complyAdvantageInstance,
  emailageInstance
}

for (const instance in instances) {

  // Request Interceptor

  instances[instance].interceptors.request.use(function (config) {
    // Do something before request is sent
    console.log('[message] - Outgoing Request', '[method] -', config.method, '[url] -', config.baseURL + config.url);
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  })

  // Response Interceptor

  instances[instance].interceptors.response.use(function (response) {
    const {
      config,
      status
    } = response
    // Do something with response data
    console.log('[message] - Incoming Response', '[method] -', config.method, '[url] -', config.url, '[status] -', status);
    return response;
  }, function (error) {
    // Do something with response error
    return Promise.reject(error);
  });
}

module.exports = instances
