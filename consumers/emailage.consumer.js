const nconf = require('nconf')
const mongoose = require('mongoose')
const queueHandler = require('../handlers/queue.handler')
const axios = require('axios')
const querystring = require('querystring')
const updateReceivedPayload = require('./shared/updateReceivedPayload')
const {
  emailageInstance
} = require('./utility/axiosInstances')

const messageParser = (message) => {
  const email = message.email_id
  const emailDomain = email.replace(/.*@/, "")
  const docket_id = message.docket.id
  const message_id = message._id + ''
  const elementalClient = message.profiles[0]

  if (nconf.get("emailage:domains").includes(emailDomain)) {
    console.log('Not creating emailage task for -', emailDomain)
    return
  }

  if (elementalClient) {
    const queueData = {
      message: message_id,
      consumed: false,
      processed: false,
      payload: {
        sent: {
          eid: message.email_id,
          docket_id,
          api_key: elementalClient.api_key,
          secret_key: elementalClient.secret_key
        },
        received: null
      },
      consumer: {
        name: 'EMAILAGE'
      }
    }

    queueHandler.toQueue(queueData, task => {
        task.payload.sent.urid = task._id + ''
        task.consumer.task = 'PROVIDE_REPORT'
      })
      .then(async data => {
        try {
          await transcribe(data)
        } catch (err) {
          console.error('error in pre post - emailage - queue id - ', data._id)
        }
      })
      .catch(err => {
        console.error('error putting task in queue - emailage - message id - ', message_id)
      })
  }
}

const transcribe = async () => {}

const provideReport = async (data, cronjob) => {
  if (cronjob) return

  try {
    const emailage_res = await emailageInstance.post(`/email_elemental.php?${querystring.stringify(data.payload.sent)}`)

    await updateReceivedPayload(data._id + '', emailage_res)
  } catch (err) {
    console.error('emailage err -', err.message, '- queue id -', data._id);

    await updateReceivedPayload(data._id + '', err.response)
  }
}

module.exports = {
  messageParser,
  provideReport
}
