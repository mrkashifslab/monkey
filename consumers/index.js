const au10tix = require('./au10tix.consumer')
const emailage = require('./emailage.consumer')
const comply_advantage = require('./comply_advantage.consumer')

module.exports = {
  au10tix,
  emailage,
  comply_advantage
}
