const QueueModel = require('mongoose').model('Queue')

module.exports = async (_id, receivedRes) => {
  const receivedPayload = {
    status: receivedRes.status,
    data: receivedRes.data
  }

  try {
    return await QueueModel.findOneAndUpdate({
      _id
    }, {
      consumed: receivedRes.status === 200,
      'payload.received': receivedPayload
    }, {
      new: true
    })
  } catch (err) {
    console.error('error updating received payload - ', err)
  }
}
