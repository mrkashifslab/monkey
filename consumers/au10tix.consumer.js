const nconf = require('nconf')
const queueHandler = require('../handlers/queue.handler')
const strConstants = require('../config/constants/microservices.' + nconf.get('NODE_ENV')).AU10TIX
const updateReceivedPayload = require('./shared/updateReceivedPayload')
const axios = require('axios')
const { au10tixInstance } = require('./utility/axiosInstances')

const messageParser = message => {
  const docket_id = message.docket.id
  const message_id = message._id + ''
  const { api_key, secret_key } = message.profiles[0]

  const elementalClient = { apiKey: api_key, secretKey: secret_key }

  const queueData = {
    message: message_id,
    consumed: false,
    processed: false,
    payload: {
      sent: null,
      received: null
    },
    consumer: {
      name: 'AU10TIX'
    }
  }

  queueData.payload.sent = {
    docketId: docket_id,
    reportProviderId: 3
  }

  message.docket.documents.forEach(doc => {
    if (strConstants[doc.document_type_id]) {
      const documentType = strConstants[doc.document_type_id]

      const payload = {
        documentTypeId: doc.document_type_id,
        autoTranscribe: doc.is_auto_transcribable
      }

      if (documentType.name.toLowerCase().includes('passport') || documentType.name.toLowerCase().includes('front')) {
        payload.frontSide = doc.path
        payload.frontSideId = doc.id
      }

      if (documentType.name.toLowerCase().includes('back')) {
        payload.backSide = doc.path
        payload.backSideId = doc.id
      }

      for (const key in payload) {
        if (!queueData.payload.sent.hasOwnProperty(key)) {
          queueData.payload.sent[key] = payload[key]
        }
      }
    }
  })

  Promise.all([
    queueHandler.toQueue(queueData, item => {
      item.payload.sent.monkeyQueue = item._id + ''
      item.payload.sent.elementalClient = elementalClient
      item.consumer.task = 'TRANSCRIBE'
    }),
    queueHandler.toQueue(queueData, item => {
      item.payload.sent.monkeyQueue = item._id + ''
      item.payload.sent.elementalClient = elementalClient
      item.consumer.task = 'PROVIDE_REPORT'
    })
  ])
    .then(async ([transcribePayload, reportPayload]) => {
      try {
        await transcribe(transcribePayload)

        return
      } catch (err) {
        console.error('error in pre post - au10tix, task - TRANSCRIBE - queue id - ', transcribePayload._id)
      }
    })
    .catch(err => {
      console.error('error putting task in queue - au10tix - message id - ', message_id)
    })

  // queueHandler
  //   .toQueue(queueData, item => {
  //     item.payload.sent.monkey_queue = item._id + ''
  //     item.payload.sent.elementalClient = elementalClient
  //   })
  //   .then(async data => {
  //     try {
  //       await transcribe(data)
  //     } catch (err) {
  //       console.error('error in pre post - au10tix - queue id - ', data._id)
  //     }
  //   })
  //   .catch(err => {
  //     console.error(
  //       'error putting task in queue - au10tix - message id - ',
  //       message_id
  //     )
  //   })
}

const transcribe = async (data, cronjob) => {
  if (cronjob) return

  try {
    const au10tix_res = await au10tixInstance.post('/monkey', data.payload.sent)

    await updateReceivedPayload(data._id + '', au10tix_res)
  } catch (err) {
    console.error('au10tix err - ', err.message)

    await updateReceivedPayload(data._id + '', err.response)
  }
}

const provideReport = async (data, cronjob) => {
  if (cronjob) return

  try {
    const au10tix_res = await au10tixInstance.post('/monkey/report', data.payload.sent)

    await updateReceivedPayload(data._id + '', au10tix_res)
  } catch (err) {
    console.error('au10tix err - ', err.message)

    await updateReceivedPayload(data._id + '', err.response)
  }
}

module.exports = {
  messageParser,
  transcribe,
  provideReport
}
