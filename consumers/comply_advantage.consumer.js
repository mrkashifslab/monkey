const nconf = require('nconf')
const mongoose = require('mongoose')
const queueHandler = require('../handlers/queue.handler')
const axios = require('axios')
const querystring = require('querystring')
const updateReceivedPayload = require('./shared/updateReceivedPayload')
const { complyAdvantageInstance } = require('./utility/axiosInstances')

const messageParser = message => {
  const docketId = message.docket.id
  const message_id = message._id + ''
  const elementalClient = message.profiles[0]

  if (elementalClient) {
    const queueData = {
      message: message_id,
      consumed: false,
      processed: false,
      payload: {
        sent: {
          searchTerm: message.name,
          docketId,
          elementalClient: {
            apiKey: elementalClient.api_key,
            secretKey: elementalClient.secret_key
          }
        },
        received: null
      },
      consumer: {
        name: 'COMPLY_ADVANTAGE'
      }
    }

    queueHandler
      .toQueue(queueData, task => {
        task.payload.sent.monkeyQueue = task._id + ''
        task.consumer.task = 'PROVIDE_REPORT'
        // task.payload.sent.elementalClient = elementalClient
      })
      .then(async data => {
        try {
          await transcribe(data)
        } catch (err) {
          console.error('error in pre post - emailage - queue id - ', data._id)
        }
      })
      .catch(err => {
        console.error('error putting task in queue - emailage - message id - ', message_id)
      })
  }
}

const transcribe = async () => {}

const provideReport = async (data, cronjob) => {
  if (cronjob) return

  try {
    const complyAdvantage_res = await complyAdvantageInstance.post('/fetchReport', data.payload.sent)

    await updateReceivedPayload(data._id + '', complyAdvantage_res)
  } catch (err) {
    console.error('comply advantage err -', err.message, '- queue id -', data._id)

    await updateReceivedPayload(data._id + '', err.response)
  }
}

module.exports = {
  messageParser,
  provideReport
}
