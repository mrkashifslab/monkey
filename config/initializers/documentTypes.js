const nconf = require('nconf')
const axios = require('axios')
const crypto = require('crypto')

const baseUrl = nconf.get('elemental:url')
const api_key = nconf.get('elemental:api_key')
const secret_key = nconf.get('elemental:secret_key')

const getDocumentTypes = async () => {
  const method = 'GET',
    path = `external_clients/active_identification_document_types`,
    url = `${baseUrl + path}`,
    tonce = new Date().getTime() + 1000,
    signature = crypto
    .createHmac('sha256', secret_key)
    .update(`${method}|${path}|api_token=${api_key}&tonce=${tonce}`)
    .digest('hex'),
    params = {
      api_token: api_key,
      tonce,
      signature
    }

  try {
    console.info(`Monkey MS → fetching document types`)

    const res = await axios[method.toLowerCase()](url, {
      params
    })
    const document_types = res.data.document_types

    // Set document types
    nconf.set('document_types', document_types)

    console.info(`Monkey MS → fetched & set document types`)

  } catch (err) {
    throw err
  }
}

module.exports = getDocumentTypes
