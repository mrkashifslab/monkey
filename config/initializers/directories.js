const fse = require('fs-extra')

const createDir = async ({ dir, options }) => {
  try {
    return await fse.ensureDir(dir, options)
  } catch (err) {
    throw err
  }
}

const createDirs = async dirs => {
  if (!(dirs instanceof Array)) throw new Error('Dirs is not an array')

  try {
    const data = await Promise.all(dirs.map(dir => createDir(dir)))
    return data
  } catch (err) {
    throw err
  }
}

module.exports = createDirs
