const express = require('express'),
  morgan = require('morgan'),
  bodyParser = require('body-parser'),
  swaggerUi = require('swagger-ui-express'),
  nconf = require('nconf'),
  swaggerDocument = require('../../docs/swagger.json'),
  routes = require('../../routes'),
  socket = require('./sockets')

swaggerDocument.host = nconf.get('url')
const app = express(),
  httpServer = require('http').createServer(app)

const start = async cb => {
  app.use(morgan('[:date[clf]] :method :url :status :res[content-length] - :response-time ms - :user-agent'))

  app.use(bodyParser.json())
  app.use(
    bodyParser.urlencoded({
      extended: true
    })
  )

  app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

  app.use('/', routes)

  try {
    const server = await httpServer.listen(nconf.get('port'))

    // setting up socket
    await socket.connect(httpServer)

    console.info(`Monkey MS → running on PORT ${server.address().port}`)

    if (cb) {
      cb(false, app)
    }

    return server
  } catch (err) {
    throw err
  }
}

module.exports = { start, app }
