const socket = require('./sockets')

describe('It verifies the socket connections and events', () => {
  beforeAll(() => {
    require('./environment')()
  })

  afterEach(async () => {
    await socket.stopConnection()
  })

  test('it should create a connection', async () => {
    const connection = await socket.connect(9999)
    expect(connection).toBe(true)
  })

  test('it should throw an exception if event not defined', async () => {
    const connection = await socket.connect(1234)
    expect(connection).toBe(true)

    const message = socket.sendMessage()
    expect(message).rejects.toThrow()
  })

  test('it should create a connection and send a message', async () => {
    const connection = await socket.connect(1234)
    expect(connection).toBe(true)

    const message = await socket.sendMessage('test', 'Hi this is a test msg')
    expect(message).toBe(true)
  })

  test('it should throw error if connection not created', () => {
    const connection = socket.connect('')
    expect(connection).rejects.toThrow()
  })
})
