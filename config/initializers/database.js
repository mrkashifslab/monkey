const mongoose = require('mongoose'),
  fs = require('fs'),
  util = require('util'),
  path = require('path'),
  nconf = require('nconf'),
  dbUrl = nconf.get('database:url')
// const ValidationError = require("../../util/validationError.util");

const readdir = util.promisify(fs.readdir)

const connectDatabase = async (
  dbUrl,
  options = {
    useNewUrlParser: true,
    useCreateIndex: true
  }
) => {
  const modelsPath = path.join(__dirname, '../../models'),
    models = await readdir(modelsPath)
  try {
    const connection = await mongoose.connect(dbUrl, options)

    console.info(`Monkey MS → connected to mongoDB`)

    console.info(`Monkey MS → Importing all database schemas`)

    models.forEach(model => {
      if (model !== `index.js` && !model.includes('spec')) require(`${modelsPath}/${model}`)
    })

    console.info(`Monkey MS → Imported all database schemas SUCCESSFULLY`)

    return true
  } catch (err) {
    throw err
  }
}

module.exports = connectDatabase
