const io = require('socket.io')
let socket

const connect = async connection => {
  try {
    if (!connection) throw new Error('Connection Not Found')
    socket = await io(connection)

    console.info(`Monkey MS → Socket connected`)

    socket.on('connection', socket => {
      console.log('Monkey MS → Socket connection found')
    })

    return true
  } catch (error) {
    throw error
  }
}

// Send message to an event from socket.io
const sendMessage = async (event, msg) => {
  try {
    if (!event || !msg) throw new Error('Event or Message not found')
    await socket.emit(event, msg)

    return true
  } catch (error) {
    throw error
  }
}

// stop connection
const stopConnection = () => {
  socket.close()
}

// For testiing
// setInterval(() => {
//   sendMessage('sendFromQueue', `hi this is test message ${Math.random()}`)
// }, 1000)

module.exports = {
  sendMessage,
  connect,
  stopConnection
}
