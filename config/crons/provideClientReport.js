const mongoose = require('mongoose')
const QueueModel = mongoose.model('Queue')
const MessageModel = mongoose.model('Message')
const elementalInstance = require('../../util/axios.util/elemental.axios')
const consumers = require('../../consumers')

const getClientMessages = async () => {
  try {
    const data = await MessageModel.find({
      'docket.status': {
        $ne: 'setup'
      },
      clientId: {
        $ne: null
      },
      profiles: {
        $elemMatch: {
          $and: [{
              api_key: {
                $ne: null
              }
            },
            {
              secret_key: {
                $ne: null
              }
            }
          ]
        }
      }
    }).lean()

    return data
  } catch (err) {
    console.error('failed to get all new messages', err)
    throw err
  }
}

const getDocketStatus = async message => {
  const docketId = message.docket.id
  const elementalClient = message.profiles[0]

  try {
    const {
      data
    } = await elementalInstance.get(`external_clients/docket/${docketId}/client_status`, {
      params: {
        id: docketId,
      },
      headers: {
        elementalClient
      }
    })

    data.client.docket.id = docketId
    data.client.docket.messageId = message._id + ''

    return data
  } catch (err) {
    throw err.response ? err.response.data : err.message
  }
}

let cron = null

const start = () => {
  const cronTime = 20 * 1000

  try {
    if (!cron) {
      console.log(`Monkey MS → Cron provide client report init -`, new Date())

      const job = async () => {
        try {
          const messages = await getClientMessages()

          console.log('Monkey MS → found # of messages -', messages.length)

          if (!messages.length) {
            stop()
            return
          }

          const docketStatusPromises = messages.map(message => getDocketStatus(message))

          const clients = await Promise.all(docketStatusPromises)

          const dockets = clients.map(client => client.client.docket).filter(docket => docket.status === 'setup')

          await MessageModel.updateMany({
            _id: {
              $in: dockets.map(docket => docket.messageId)
            }
          }, {
            $set: {
              'docket.status': 'setup'
            }
          })

          let tasks = await QueueModel.find({
            message: {
              $in: dockets.map(docket => docket.messageId)
            },
            'consumer.task': 'PROVIDE_REPORT'
          })

          tasks = tasks.map(task => task._doc)

          tasks.forEach(task => consumers[task.consumer.name.toLowerCase()].provideReport(task))
        } catch (err) {
          throw err
        }
      }

      job().catch(err => console.log('err in provideClientReport cronjob -', err))

      cron = setInterval(job, cronTime)
    }
  } catch (err) {
    console.error('error start cron', err)
  }
}

const stop = () => {
  if (cron) {
    console.log(`Monkey MS → Cron provide client report stop -`, new Date())
    clearInterval(cron)
    cron = null
  }
}

module.exports = {
  start,
  stop,
  getClientMessages
}