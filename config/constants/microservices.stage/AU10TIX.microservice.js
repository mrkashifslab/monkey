module.exports = {
  "53": {
    "name": "Passport Fazremit Test",
    "keys": {
      "0": {
        "name": "ExpirationDate",
        "is_pii_key": "false",
        "data_type_id": "5"
      },
      "1": {
        "name": "IssueCountry",
        "is_pii_key": "false",
        "data_type_id": "1"
      },
      "2": {
        "name": "IssueDate",
        "is_pii_key": "false",
        "data_type_id": "5"
      },
      "3": {
        "name": "IssuedBy",
        "is_pii_key": "false",
        "data_type_id": "2"
      },
      "4": {
        "name": "Number",
        "is_pii_key": "false",
        "data_type_id": "2"
      }
    },
    "is_active": true,
    "created_at": "2018-12-25T11:13:29.707Z",
    "updated_at": "2018-12-25T11:13:29.707Z",
    "country_id": 247,
    "used_for": "identification",
    "is_auto_transcribable": false,
    "country_name": "All Countries"
  },
  "54": {
    "name": "Driving License front Fazremit Test",
    "keys": {
      "0": {
        "name": "ExpirationDate",
        "is_pii_key": "false",
        "data_type_id": "5"
      },
      "1": {
        "name": "IssueCountry",
        "is_pii_key": "false",
        "data_type_id": "1"
      },
      "2": {
        "name": "Number",
        "is_pii_key": "false",
        "data_type_id": "2"
      },
      "3": {
        "name": "IssueDate",
        "is_pii_key": "false",
        "data_type_id": "5"
      },
      "4": {
        "name": "IssuedBy",
        "is_pii_key": "false",
        "data_type_id": "2"
      }
    },
    "is_active": true,
    "created_at": "2018-12-25T11:15:40.800Z",
    "updated_at": "2018-12-25T11:15:40.800Z",
    "country_id": 247,
    "used_for": "identification",
    "is_auto_transcribable": false,
    "country_name": "All Countries"
  },
  "55": {
    "name": "Driving License back Fazremit Test",
    "keys": {
      "0": {
        "name": "ExpirationDate",
        "is_pii_key": "false",
        "data_type_id": "5"
      },
      "1": {
        "name": "IssueCountry",
        "is_pii_key": "false",
        "data_type_id": "1"
      },
      "2": {
        "name": "Number",
        "is_pii_key": "false",
        "data_type_id": "2"
      },
      "3": {
        "name": "IssueDate",
        "is_pii_key": "false",
        "data_type_id": "5"
      },
      "4": {
        "name": "IssuedBy",
        "is_pii_key": "false",
        "data_type_id": "2"
      }
    },
    "is_active": true,
    "created_at": "2018-12-25T11:17:23.124Z",
    "updated_at": "2018-12-25T11:17:23.124Z",
    "country_id": 247,
    "used_for": "identification",
    "is_auto_transcribable": false,
    "country_name": "All Countries"
  },
  "56": {
    "name": "State issued identity card front Fazremit Test",
    "keys": {
      "0": {
        "name": "ExpirationDate",
        "is_pii_key": "false",
        "data_type_id": "5"
      },
      "1": {
        "name": "IssueDate",
        "is_pii_key": "false",
        "data_type_id": "5"
      },
      "2": {
        "name": "IssueCountry",
        "is_pii_key": "false",
        "data_type_id": "1"
      },
      "3": {
        "name": "Number",
        "is_pii_key": "false",
        "data_type_id": "2"
      },
      "4": {
        "name": "IssuedBy",
        "is_pii_key": "false",
        "data_type_id": "2"
      }
    },
    "is_active": true,
    "created_at": "2018-12-25T11:20:03.932Z",
    "updated_at": "2018-12-25T11:20:03.932Z",
    "country_id": 247,
    "used_for": "identification",
    "is_auto_transcribable": false,
    "country_name": "All Countries"
  },
  "57": {
    "name": "State issued identity card back Fazremit Test",
    "keys": {
      "0": {
        "name": "ExpirationDate",
        "is_pii_key": "false",
        "data_type_id": "5"
      },
      "1": {
        "name": "IssueCountry",
        "is_pii_key": "false",
        "data_type_id": "1"
      },
      "2": {
        "name": "IssueDate",
        "is_pii_key": "false",
        "data_type_id": "5"
      },
      "3": {
        "name": "Number",
        "is_pii_key": "false",
        "data_type_id": "2"
      },
      "4": {
        "name": "IssuedBy",
        "is_pii_key": "false",
        "data_type_id": "2"
      }
    },
    "is_active": true,
    "created_at": "2018-12-25T11:21:29.620Z",
    "updated_at": "2018-12-25T11:21:29.620Z",
    "country_id": 247,
    "used_for": "identification",
    "is_auto_transcribable": false,
    "country_name": "All Countries"
  },
  "32": {
    "name": "All Countries - PASSPORT (auto-transcribable)"
  }
}
