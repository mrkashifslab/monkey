module.exports = {
  "1": {
    "name": "Passport",
    "keys": {
      "0": {
        "name": "ExpirationDate",
        "is_pii_key": "true",
        "data_type_id": "5"
      },
      "1": {
        "name": "IssueCountry",
        "is_pii_key": "true",
        "data_type_id": "2"
      },
      "2": {
        "name": "Number",
        "is_pii_key": "true",
        "data_type_id": "2"
      },
      "3": {
        "name": "IssueDate",
        "is_pii_key": "true",
        "data_type_id": "5"
      },
      "4": {
        "name": "IssuedBy",
        "is_pii_key": "true",
        "data_type_id": "2"
      }
    },
    "is_active": true,
    "created_at": "2018-12-27T13:12:21.910Z",
    "updated_at": "2019-01-07T07:59:25.229Z",
    "country_id": 247,
    "used_for": "identification",
    "country_name": "All Countries"
  },
  "2": {
    "name": "Driving License front",
    "keys": {
      "0": {
        "name": "ExpirationDate",
        "is_pii_key": "true",
        "data_type_id": "5"
      },
      "1": {
        "name": "IssueCountry",
        "is_pii_key": "true",
        "data_type_id": "2"
      },
      "2": {
        "name": "Number",
        "is_pii_key": "true",
        "data_type_id": "2"
      },
      "3": {
        "name": "IssueDate",
        "is_pii_key": "true",
        "data_type_id": "5"
      },
      "4": {
        "name": "IssuedBy",
        "is_pii_key": "true",
        "data_type_id": "2"
      }
    },
    "is_active": true,
    "created_at": "2018-12-27T13:16:26.628Z",
    "updated_at": "2019-01-07T07:59:27.725Z",
    "country_id": 247,
    "used_for": "identification",
    "country_name": "All Countries"
  },
  "3": {
    "name": "Driving License back",
    "keys": {
      "0": {
        "name": "ExpirationDate",
        "is_pii_key": "true",
        "data_type_id": "5"
      },
      "1": {
        "name": "IssueCountry",
        "is_pii_key": "true",
        "data_type_id": "2"
      },
      "2": {
        "name": "Number",
        "is_pii_key": "true",
        "data_type_id": "2"
      },
      "3": {
        "name": "IssueDate",
        "is_pii_key": "true",
        "data_type_id": "5"
      },
      "4": {
        "name": "IssuedBy",
        "is_pii_key": "true",
        "data_type_id": "2"
      }
    },
    "is_active": true,
    "created_at": "2018-12-27T13:21:56.644Z",
    "updated_at": "2019-01-07T07:59:30.216Z",
    "country_id": 247,
    "used_for": "identification",
    "country_name": "All Countries"
  },
  "4": {
    "name": "State issued identity card front",
    "keys": {
      "0": {
        "name": "ExpirationDate",
        "is_pii_key": "true",
        "data_type_id": "5"
      },
      "1": {
        "name": "IssueCountry",
        "is_pii_key": "true",
        "data_type_id": "2"
      },
      "2": {
        "name": "Number",
        "is_pii_key": "true",
        "data_type_id": "2"
      },
      "3": {
        "name": "IssueDate",
        "is_pii_key": "true",
        "data_type_id": "5"
      },
      "4": {
        "name": "IssuedBy",
        "is_pii_key": "true",
        "data_type_id": "2"
      }
    },
    "is_active": true,
    "created_at": "2018-12-27T13:27:53.502Z",
    "updated_at": "2019-01-07T07:59:32.655Z",
    "country_id": 247,
    "used_for": "identification",
    "country_name": "All Countries"
  },
  "5": {
    "name": "State issued identity card back",
    "keys": {
      "0": {
        "name": "ExpirationDate",
        "is_pii_key": "true",
        "data_type_id": "5"
      },
      "1": {
        "name": "IssueCountry",
        "is_pii_key": "true",
        "data_type_id": "2"
      },
      "2": {
        "name": "Number",
        "is_pii_key": "true",
        "data_type_id": "2"
      },
      "3": {
        "name": "IssueDate",
        "is_pii_key": "true",
        "data_type_id": "5"
      },
      "4": {
        "name": "IssuedBy",
        "is_pii_key": "true",
        "data_type_id": "2"
      }
    },
    "is_active": true,
    "created_at": "2018-12-27T13:29:32.863Z",
    "updated_at": "2019-01-07T07:59:35.309Z",
    "country_id": 247,
    "used_for": "identification",
    "country_name": "All Countries"
  },
  "60": {
    "name": "All Countries - PASSPORT(auto-transcribable)"
  }
}
