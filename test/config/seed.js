const downloadFile = require('../../util/downloadFile.util')
const fs = require('fs-extra')

;(async () => {
  try {
    await require('../../config/initializers/directories')([
      { dir: 'test/assets/temp' },
      { dir: 'test/assets/receive' }
    ])

    await Promise.all(
      [
        downloadFile(
          {
            url: 'https://i.imgur.com/G4S4tCg.jpg',
            path: 'test/assets/temp/',
            fileName: 'passport'
          },
          { fileType: true, fileSize: true }
        )
      ],
      [
        downloadFile(
          {
            url: 'https://i.imgur.com/G4S4tCg.jpg',
            path: 'test/assets/receive/',
            fileName: 'passport1.pdf'
          },
          { fileType: true, fileSize: true }
        )
      ]
    )

    console.info('Seed data - complete')
  } catch (err) {
    console.error('error while seeding test', err)
  }
})()
