const fs = require("fs");
const { pipeline } = require("stream");
const express = require("express");
const fileController = require("../../controllers/file.controller");
const app = express();

app.post("/api/file", fileController.uploadFile);

class MockServer {
  constructor(port) {
    this.port = port;
  }

  async start() {
    if (this.server)
      throw new Error(
        `Server already running at port ${this.server.address().port}`
      );
    this.app = app;

    this.server = await app.listen(this.port);

    this.port = this.server.address().port;

    this._warning = setInterval(
      () =>
        console.warn(
          `MockServer running on port ${this.port}, you forgot to stop it`
        ),
      1000
    );
  }

  async stop() {
    if (!this.server) throw new Error(`Server not running`);
    await this.server.close();
    clearInterval(this._warning);
  }
}

module.exports = MockServer;
