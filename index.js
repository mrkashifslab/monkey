const nconf = require('nconf')

// Set up configs
nconf.use('memory')

// Load environment variables
nconf.env()
;(async function() {
  try {
    console.info('Monkey MS → setting environment')

    require('./config/initializers/environment')()

    await require('./config/initializers/directories')(
      Object.values(nconf.get('dirs')).map(dir => {
        return { dir }
      })
    )

    console.info('Monkey MS → starting initialization')

    const database = require('./config/initializers/database')
    await database(nconf.get('database:url'), {
      useNewUrlParser: true,
      useCreateIndex: true
    })

    // const getDocumentTypes = require('./config/initializers/documentTypes')
    // await getDocumentTypes()

    // Add migration here

    // Start crons
    require('./config/crons/provideClientReport').start()

    const { start } = require('./config/initializers/server')
    await start()

    console.info(`Monkey MS → initialized SUCCESSFULLY`)
  } catch (err) {
    console.error(`Monkey MS → failed to initialize`, err)
  }
})()
