const nconf = require('nconf')
const axios = require('axios')
const crypto = require('crypto')

const baseURL = nconf.get('elemental:url')

const toQueryString = (queryObj, nesting = '') => {
  const pairs = Object.entries(queryObj).map(([key, value]) => {
    if (typeof value === 'object') return `${key}=${JSON.stringify(value)}`
    else return `${key}=${value}`
  })

  return pairs.join('&')
}

const authenticateReq = config => {
  const {
    api_key,
    secret_key
  } = config.headers.elementalClient

  delete config.headers.elementalClient

  const method = config.method.toUpperCase()
  const path = config.url.replace(baseURL, '')
  const tonce = new Date().getTime() + 1000
  const params = {
    api_token: api_key,
    ...config.params,
    ...config.data,
    tonce
  }

  // create signature

  const signature = crypto
    .createHmac('sha256', secret_key)
    .update(`${method}|${path}|${toQueryString(params)}`)
    .digest('hex')

  // Append signature to request params

  params.signature = signature

  // Sanatize request params for included data

  if (config.data) {
    for (const key in config.data) {
      delete params[key]
    }
  }

  // reassign config params with newly created params

  config.params = params
}

const elementalInstance = axios.create({
  baseURL
})

elementalInstance.interceptors.request.use(config => {
  authenticateReq(config)
  return config
})

module.exports = elementalInstance