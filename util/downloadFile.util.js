const env = process.env.NODE_ENV;
const fs = require("fs");
const util = require("util");
const stream = require("stream");
const uuidv4 = require("uuid/v4");
const fileType = require("file-type");
const axios = require("axios");
const pipeline = util.promisify(stream.pipeline);

class FileError extends Error {
  constructor(message, url, status) {
    super(message);
    this.name = this.constructor.name;
    this.url = url;
    this.status = status;
  }
}

const download = async ({ url, path, fileName, extraInfo }, config = {}) => {
  path = path[path.length - 1] === "/" ? path : path + "/";

  try {
    const res = await axios.get(url, {
      responseType: "stream"
    });

    const stream = await fileType.stream(res.data);
    const name = fileName || `${uuidv4()}.${stream.fileType.ext}`;
    const fullPath = `${path || ""}${name}`;
    const supportedFileTypes = ["jpg", "tiff", "pdf"];
    const mb = 3.9;
    let kb, by;
    kb = by = 1024;
    const maxFileSize = mb * kb * by;
    const fileSize = res.headers["content-length"];

    // validation
    if (!supportedFileTypes.includes(stream.fileType.ext) && !config.fileType) {
      throw new FileError(`Unsupported file type - ${stream.fileType}`, url, 406);
    }

    if (fileSize > maxFileSize && !config.fileSize) {
      throw new FileError(`File exceeds maximum file-size of 3.9MB. Size of file - ${fileSize / kb / by}MB`, url, 406);
    }

    await pipeline(stream, fs.createWriteStream(fullPath));

    return {
      name,
      path,
      fullPath,
      extraInfo
    };
  } catch (err) {
    throw err;
  }
};

module.exports = download;
